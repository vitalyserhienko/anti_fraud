## Antifraud Stream Simulator

### Installation

##### If docker and docker-compose is not installed
```bash
sudo apt-get update
sudo apt-get install docker-ce
sudo apt-get install docker-compose
```
Do this:
```bash
sudo usermod -aG docker ${USER}
```
Or run next commands with sudo - as you wish.

##### If docker and docker-compose is installed
```bash
docker-compose build
docker-compose up
```
Go to browser
```http request
http://127.0.0.1:8000/notify/
```
Wait for message!